# Outils de développement

## Warnings

L'activation des *warnings* est la première chose à faire pour éviter des erreurs.
Ils vous permetteront d'obtenir des informations sur des erreurs de code sans même devoir exécuté le programme.

Pour activer un *warning* il suffit d'ajouter des options à la ligne de commande que vous utilisez.
Les options commencant par `-W` activent un *warning*, celles par `-w`les désactivent.

On vous recommande d'utiliser les warnings suivant avec gcc et clang :
 - `-Wall` et `-Wextra`: Ces deux options activent des groupes de *warnings* (plus d'informations dans le `man`)
 - `-Wshadow` : Affiche un warning lorsqu'une variable porte le même nom qu'une autre dans le scope au dessus.
 - `-Werror` : Celui-ci transforme les warnings en erreurs, vous forçant à les corriger avant de pouvoir réussir une compilation.

Attention, les options d'activations des *warnings* peuvent changer selon le compilateur ainsi que selon la version de celui-ci.


## Analyse statique

De nombreuses erreurs ne sont pas détectables par de simples *warnings*, on utilise alors des outils d'analyse statique.
Nous nous concentrons ici sur les *sanitizers* intégrés aux compilateurs car ils sont très simples d'accès.
Sachez qu'il est commun de trouver d'autres outils en entreprise tels que *SonarQube* (multi language) ou bien *cppcheck* (orienté C et C++)

Les *sanitizers* sont des outils activables par une simple option de compilation `-fsanitize=<nom_sanitizer>`.
Lors de l'activation d'un *sanitizer* le compilateur va ajouter dans l'exécutable des instructions permettant de faire des vérifications lors de l'exécution.
Cet ajout entraine nécessairement des pertes de performance et vont ralentir l'exécution de votre programme.

Il est recommandé d'ajouter le `-g` afin d'obtenir les informations précises sur la localisation des erreurs dans le code.
Attention aux options d'optimisations pouvant empêcher la détection des erreurs.


### Address Sanitizer (ASAN)

Ce *sanitizer* permet de trouver plusieurs erreurs liées à la gestion de la mémoire, telles que :
 - utilisation d'un pointeur après l'appel de `free` ;
 - dépassement de taille allouée ;
 - fuite mémoire.

Voici trois exemples simples de cas où le *sanitizer* peut apporter de l'aide.
Pour reproduire en local il faut ajouter l'option `-fsanitize=address`.


TODO: inclure address_sanitizer/use_after_free.c & .log

L'erreur commence par le texte en rouge indiquant l'erreur "heap-use-after-free" (ici heap car la mémoire est allouée avec `malloc`).
En bleu on obtient du contexte sur l'accès non autorisé, ici une lecture d'1 octet.
On a ensuite deux paragraphes commencant par une phrase en rose.
Ceux-ci indiquent chacun une pile d'appel (suite d'appels de fonctions) jusqu'à votre fichier source (ici use_after_free.c:5, le ":5" signifie ligne n°5).
La première indique où la mémoire a été libérée, la seconde où elle avait été allouée.
Enfin dans la ligne commençant par "SUMMARY" vous trouverez où vous avez essayer d'accéder à la mémoire après avoir fait la libération.


TODO: inclure address_sanitizer/overflow.c & .log

Ici aussi l'erreur commence par un texte en rouge indiquant "heap-buffer-overflow".
En bleu, on peut lire que c'est une écriture de taille 4 octets (ici `sizeof(int)` = 4).
On a ensuite la pile d'appel indiquant que l'erreur vient de overflow.c:6.
On sait également (après le texte en rose) que l'allocation avait été faite dans overflow.c:4.


TODO: inclure address_sanitizer/leak.c & .log

Dans ce dernier exemple, on peut voir en bleu les deux fuites présentes dans l'exécutable.
Les deux viennent du fichier address_sanitizer/leak.c (aux lignes 4 et 5).

### Undefined Behaviour Sanitizer (UBSAN)

Celui-ci active plusieurs *sanitizers* qui peuvent être activé individuellement.
Elles sont toutes issues de cas dis "Undefined Behaviour (UB)".
Ce sont des cas où le standard à décider de ne pas spécifier le comportement à suivre.
On trouvera ici des erreurs comme :
 - division par 0 ;
 - déréférencer un pointeur NULL ;
 - dépassement de la taille maximale d'un entier (ex: INT_MAX + 1) ;
 - dépassement de tableaux.

TODO: inclure undefined_sanitizer/integer_overflow.c

Indiquera lors de l'exécution:

TODO: inclure undefined_sanitizer/integer_overflow.log

Signifiant qu'à la ligne 5 de integer_overflow.c, le résultat d'une addition ne peut pas être représenté par une variable de type `int`.


### Thread Sanitizer (TSAN)

Ce sanitizer n'est utile que si vous développez un programme qui utilise plusieurs processus légers (*threads*).
Il permet de s'assurer qu'aucun *thread* n'essaie d'écrire une variable qui serait en même temps lue par un autre *thread*.

## Analyse dynamique



### Valgrind
