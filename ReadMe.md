# Sanitizers

Pour compiler et produire les fichiers de log il suffit de `make`.
J'ai testé sur GCC 13, malheuresement le ThreadSanitizer ne fonctionne pas chez moi... (une histoire d'aslr que je ne veux pas modifier sur cette machine).

Le reste semble correcte j'ai ajouté autant d'explications que possibles.
Je pense qu'il faut insérer tout ça juste avant le chapitre valgrind que je renommerais en "Outils de développements".

On peut aussi ajouter un chapitre sur gdb je pense.
Enfin j'ai trouvé une typo dans Installation de la SDL tu as le style des flags Og et g qui est mal passé.
