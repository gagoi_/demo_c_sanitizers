ALL: address_sanitizer undefined_sanitizer thread_sanitizer

address_sanitizer: address_sanitizer/use_after_free address_sanitizer/overflow address_sanitizer/leak
undefined_sanitizer: undefined_sanitizer/integer_overflow
thread_sanitizer: thread_sanitizer/race_condition

address_sanitizer/use_after_free: address_sanitizer/use_after_free.c
	@mkdir -p build/address_sanitizer
	${CC} -o build/$@ $^ -fsanitize=address -g
	ASAN_OPTIONS="color=always" ./build/$@ 2> ./build/$@.log | true

address_sanitizer/overflow: address_sanitizer/overflow.c
	@mkdir -p build/address_sanitizer
	${CC} -o build/$@ $^ -fsanitize=address -g
	ASAN_OPTIONS="color=always" ./build/$@ 2> ./build/$@.log | true

address_sanitizer/leak: address_sanitizer/leak.c
	@mkdir -p build/address_sanitizer
	${CC} -o build/$@ $^ -fsanitize=address -g
	ASAN_OPTIONS="color=always" ./build/$@ 2> ./build/$@.log | true

undefined_sanitizer/integer_overflow: undefined_sanitizer/integer_overflow.c
	@mkdir -p build/undefined_sanitizer
	${CC} -o build/$@ $^ -fsanitize=undefined -g
	UBSAN_OPTIONS="color=always" ./build/$@ 2> ./build/$@.log | true

thread_sanitizer/race_condition: thread_sanitizer/race_condition.c
	@mkdir -p build/thread_sanitizer
	${CC} -o build/$@ $^ -fsanitize=thread -g
	TSAN_OPTIONS="color=always verbosity=3" ./build/$@ 2> ./build/$@.log | true
