#include <pthread.h>
#include <stdio.h>

int a;

void *threadfunc(void * args) {
	(void) args;

	for (int i = 0; i < 1000; ++i) {
		a += i;
	}

	return NULL;
}

int main() {
  pthread_t t;
  pthread_create(&t, 0, threadfunc, NULL);
  printf("foo=%d\n", a);
  pthread_join(t, 0);
}
